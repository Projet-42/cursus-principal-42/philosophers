/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosophers.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nabitbol <nabitbol@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/06 17:19:12 by nabitbol          #+#    #+#             */
/*   Updated: 2021/10/21 18:48:15 by nabitbol         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILOSOPHERS_H
# define PHILOSOPHERS_H

# include <stdlib.h>
# include <pthread.h>
# include <unistd.h>
# include <stdio.h>
# include <sys/time.h>

enum e_philosopher_status
{
	EAT,
	THINK,
	SLEEP,
	TAKES_FORK
};

enum e_error_messages {
	NO_ERROR,
	TOO_MANY_ARGS,
	TOO_FEW_ARGS,
	NOT_A_DIGIT,
	MEMORY_ALLOCATION_FAILED,
	MAX_INTEGER
};

enum e_unlock {
	DO_NOT_UNLOCK,
	UNLOCK
};

typedef int	t_bool;
# define FALSE 0
# define TRUE 1

typedef struct s_args
{
	int					number_of_philosophers;
	int					time_to_die;
	int					time_to_eat;
	int					time_to_sleep;
	t_bool				handle_eat_limit;
	int					number_of_times_each_philosopher_must_eat;
	long long			startup_timestamp;
}						t_args;

typedef struct s_philosopher_state
{
	int					thread_id;
	char				status;
	int					number_of_times_philosopher_ate;
	pthread_t			pthread;
	void				*program_state;
	t_bool				death;
	t_bool				everyone_ate;
	pthread_mutex_t		fork;
	pthread_mutex_t		print;
	pthread_mutex_t		protect_eat_value;
	long long			last_meal_timestamp;
}						t_philosopher_state;

typedef struct s_error
{
	t_bool				occured;
	unsigned char		message;
}						t_error;

typedef struct s_program_state
{
	t_args				args;
	int					current_philospher_index;
	int					count_death_print;
	t_philosopher_state	*philosopher_states;
	pthread_mutex_t		protect_value;
	pthread_mutex_t		protect_death_value;
	t_error				error;
}						t_program_state;

long unsigned int	ft_atoi(const char *str);
int					ft_isnumstr(char *str);
t_bool				has_errored(t_error error);
void				set_error(t_error *error, char message);
int					ms_to_us(int ms);
int					s_to_ms(int s);
int					us_to_ms(int us);
long long			get_timestamp(void);
void				ft_usleep_eat(t_program_state program_state,
						t_philosopher_state *philosopher_state);
void				ft_usleep_sleep(t_program_state program_state);

void				set_error_on_invalid_args(int ac, char **av,
						t_error *error);
t_args				parse_args(int ac, char **av);

void				init(t_program_state *program_state, int ac, char **av);

void				cleanup(t_program_state *program_state);

void				simulate_philosophers_lives(t_program_state *program_state);

void				*simulate_philosopher_life(void *args);

void				print(int id, int action,
						t_program_state *program_state);

void				philo_takes_fork(t_philosopher_state *philosopher_state,
						t_program_state *program_state);
void				philo_think(t_philosopher_state *philosopher_state,
						t_program_state *program_state);
void				philo_eat(t_philosopher_state *philosopher_state,
						t_program_state *program_state);
void				philo_sleep(t_philosopher_state *philosopher_state,
						t_program_state *program_state);

t_bool				lock_fork_unpair(t_program_state *program_state,
						t_philosopher_state *philosopher_state,
						int next_philosopher_id);
t_bool				lock_fork_pair(t_program_state *program_state,
						t_philosopher_state *philosopher_state,
						int next_philosopher_id);

void				handle_action(t_philosopher_state *philosopher_state,
						t_program_state *program_state, char action);

int					get_next_philospher_id(
						t_philosopher_state *philosopher_state,
						t_program_state program_state);
void				unlock_fork(t_philosopher_state *philosopher_state,
						t_program_state program_state);
t_bool				is_dead(t_philosopher_state philosopher_state,
						t_program_state program_state);

t_bool				is_philo_died(t_program_state *program_state);

t_bool				handle_philosopher_death(t_program_state *program_state,
						int i);

t_bool				does_everyone_ate(t_program_state *program_state,
						t_bool everyone_ate);

t_bool				everyone_ates(t_program_state *program_state);

t_bool				end_simulate_philospher_life(
						t_philosopher_state *philosopher_state,
						t_program_state *program_state,
						int unlock);

#endif
