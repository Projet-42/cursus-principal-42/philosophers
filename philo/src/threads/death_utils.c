/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   death_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nabitbol <nabitbol@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/10 09:40:21 by nabitbol          #+#    #+#             */
/*   Updated: 2021/10/21 18:43:51 by nabitbol         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/philosophers.h"

t_bool	is_philo_died(t_program_state *program_state)
{
	t_bool	tmp_death_value;

	pthread_mutex_lock(&program_state->protect_death_value);
	tmp_death_value = program_state->philosopher_states->death;
	pthread_mutex_unlock(&program_state->protect_death_value);
	if (tmp_death_value == TRUE)
		return (TRUE);
	return (FALSE);
}

t_bool	handle_philosopher_death(t_program_state *program_state, int i)
{
	if (!is_philo_died(program_state))
	{
		pthread_mutex_lock(&program_state->protect_death_value);
		program_state->philosopher_states->death = TRUE;
		pthread_mutex_unlock(&program_state->protect_death_value);
	}
	if (is_philo_died(program_state) && program_state->count_death_print == 0)
	{
		pthread_mutex_lock(&program_state->philosopher_states->print);
		printf("%-6lld %d %s\n", get_timestamp() - (program_state)->args
			.startup_timestamp, i + 1, "died");
		pthread_mutex_unlock(&program_state->philosopher_states->print);
		program_state->count_death_print += 1;
	}
	return (TRUE);
}

t_bool	is_dead(t_philosopher_state philosopher_state,
			t_program_state program_state)
{
	if (((get_timestamp() - philosopher_state.last_meal_timestamp))
		> program_state.args.time_to_die)
		return (TRUE);
	return (FALSE);
}
