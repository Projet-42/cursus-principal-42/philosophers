/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosopher_life_simulation.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nabitbol <nabitbol@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/08 16:52:41 by nabitbol          #+#    #+#             */
/*   Updated: 2021/10/21 18:40:38 by nabitbol         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/philosophers.h"

t_bool	end_simulate_philospher_life(t_philosopher_state *philosopher_state,
	t_program_state *program_state, int unlock)
{
	int	next_philosopher_id;

	next_philosopher_id = get_next_philospher_id(philosopher_state,
			*program_state);
	if (is_philo_died(program_state) || everyone_ates(program_state)
		|| program_state->args.number_of_philosophers == 1)
	{
		if (unlock == UNLOCK)
		{
			if (program_state->args.number_of_philosophers == 1)
			{
				pthread_mutex_unlock(&program_state->philosopher_states
				[next_philosopher_id].fork);
			}
			else
				unlock_fork(philosopher_state, *program_state);
		}
		return (TRUE);
	}
	return (FALSE);
}

t_bool	simulation_loop_condition_true(t_program_state *program_state,
			t_philosopher_state *philosopher_state)
{
	if (!is_philo_died(program_state) || !everyone_ates(program_state)
		|| ((program_state->args.handle_eat_limit != -1)
			&& (philosopher_state[philosopher_state->thread_id]
				.number_of_times_philosopher_ate
				< program_state->args
				.number_of_times_each_philosopher_must_eat)))
		return (TRUE);
	return (FALSE);
}

void	philosopher_life_loop(t_program_state *program_state,
			t_philosopher_state *philosopher_state)
{
	while (simulation_loop_condition_true(program_state, philosopher_state))
	{
		philo_think(philosopher_state, program_state);
		if (end_simulate_philospher_life(philosopher_state,
				program_state, DO_NOT_UNLOCK))
			break ;
		philo_takes_fork(philosopher_state, program_state);
		if (end_simulate_philospher_life(philosopher_state,
				program_state, UNLOCK))
			break ;
		philo_eat(philosopher_state, program_state);
		if (end_simulate_philospher_life(philosopher_state,
				program_state, UNLOCK))
			break ;
		philo_sleep(philosopher_state, program_state);
	}
}

/*
**	A part of code to put before philosopher_life_loop .
**	This code fix the processor decisions in the eat_limit simulation.
**	if (philosopher_state->thread_id % 2 == 0)
**	usleep(s_to_ms(program_state->args.time_to_eat / 2));
*/

void	*simulate_philosopher_life(void *args)
{
	t_program_state		*program_state;
	t_philosopher_state	*philosopher_state;

	philosopher_state = (t_philosopher_state *)args;
	program_state = (t_program_state *)philosopher_state->program_state;
	philosopher_life_loop(program_state, philosopher_state);
	return (NULL);
}
