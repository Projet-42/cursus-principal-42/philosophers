/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_fork_lock.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nabitbol <nabitbol@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/16 23:52:53 by nabitbol          #+#    #+#             */
/*   Updated: 2021/10/21 18:47:37 by nabitbol         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/philosophers.h"

t_bool	lock_fork_unpair(t_program_state *program_state,
		t_philosopher_state *philosopher_state, int next_philosopher_id)
{
	pthread_mutex_lock(&philosopher_state->fork);
	handle_action(philosopher_state, program_state, TAKES_FORK);
	if (program_state->args.number_of_philosophers == 1)
		return (FALSE);
	pthread_mutex_lock(&program_state->philosopher_states
	[next_philosopher_id].fork);
	handle_action(philosopher_state, program_state, TAKES_FORK);
	return (TRUE);
}

t_bool	lock_fork_pair(t_program_state *program_state,
		t_philosopher_state *philosopher_state, int next_philosopher_id)
{
	pthread_mutex_lock(&program_state->philosopher_states
	[next_philosopher_id].fork);
	if (program_state->args.number_of_philosophers == 1)
		return (FALSE);
	handle_action(philosopher_state, program_state, TAKES_FORK);
	pthread_mutex_lock(&philosopher_state->fork);
	handle_action(philosopher_state, program_state, TAKES_FORK);
	return (TRUE);
}
