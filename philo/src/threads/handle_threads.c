/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_threads.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nabitbol <nabitbol@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/10 09:38:52 by nabitbol          #+#    #+#             */
/*   Updated: 2021/10/21 18:46:57 by nabitbol         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/philosophers.h"

void	create_threads(t_program_state *program_state)
{
	int	index;

	index = 0;
	while (index < program_state->args.number_of_philosophers)
	{
		pthread_create(&program_state->philosopher_states[index].pthread, NULL,
			simulate_philosopher_life,
			&program_state->philosopher_states[index]);
		index++;
	}
}

void	wait_threads_end(t_program_state *program_state)
{
	int	i;

	i = program_state->args.number_of_philosophers - 1;
	while (i >= 0)
	{
		pthread_join(program_state->philosopher_states[i].pthread, NULL);
		i--;
	}
}

void	*end_watcher(void *program_state_void)
{
	int				i;
	t_bool			someone_died;
	t_bool			everyone_ate;
	t_program_state	*program_state;

	everyone_ate = FALSE;
	someone_died = FALSE;
	program_state = (t_program_state *)program_state_void;
	while (!someone_died && !everyone_ate)
	{
		i = 0;
		everyone_ate = TRUE;
		while (i < program_state->args.number_of_philosophers)
		{
			if (is_dead((program_state)->philosopher_states[i],
					*(program_state)))
				someone_died = handle_philosopher_death(program_state, i);
			everyone_ate = does_everyone_ate(program_state, everyone_ate);
			i++;
		}
	}
	if (someone_died || everyone_ate)
		wait_threads_end((program_state));
	return (NULL);
}

void	simulate_philosophers_lives(t_program_state *program_state)
{
	pthread_t	pthread;

	create_threads(program_state);
	pthread_create(&pthread, NULL, end_watcher, program_state);
	pthread_join(pthread, NULL);
}
