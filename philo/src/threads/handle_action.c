/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_action.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nabitbol <nabitbol@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/09 14:32:34 by nabitbol          #+#    #+#             */
/*   Updated: 2021/10/21 18:37:40 by nabitbol         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/philosophers.h"

void	handle_action(t_philosopher_state *philosopher_state,
					t_program_state *program_state, char action)
{
	struct timeval	current_time;

	gettimeofday(&current_time, NULL);
	print(philosopher_state->thread_id, action, program_state);
	if (action == EAT)
		ft_usleep_eat(*program_state, philosopher_state);
	if (action == SLEEP)
	{
		unlock_fork(philosopher_state, *program_state);
		ft_usleep_sleep(*program_state);
	}
}
