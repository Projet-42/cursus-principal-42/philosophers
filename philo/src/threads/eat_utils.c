/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   eat_utils.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nabitbol <nabitbol@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 16:31:03 by nabitbol          #+#    #+#             */
/*   Updated: 2021/10/21 15:30:55 by nabitbol         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/philosophers.h"

t_bool	philosopher_ate_enough(t_program_state *program_state, int i)
{
	int	number_of_time_each_philospher_ate;

	pthread_mutex_lock(&program_state->philosopher_states->protect_eat_value);
	number_of_time_each_philospher_ate = program_state->philosopher_states[i]
		.number_of_times_philosopher_ate;
	pthread_mutex_unlock(&program_state->philosopher_states->protect_eat_value);
	if (number_of_time_each_philospher_ate >= program_state->args
		.number_of_times_each_philosopher_must_eat)
		return (TRUE);
	return (FALSE);
}

t_bool	every_philosopher_ate_enough(t_program_state *program_state)
{
	int		i;
	t_bool	everyone_ate;

	i = 0;
	everyone_ate = TRUE;
	while (i < program_state->args.number_of_philosophers)
	{
		if (!everyone_ate || !philosopher_ate_enough(program_state, i))
			everyone_ate = FALSE;
		i++;
	}
	return (everyone_ate);
}

t_bool	does_everyone_ate(t_program_state *program_state,
			t_bool everyone_ate)
{
	pthread_mutex_lock(&program_state->philosopher_states->print);
	if ((program_state)->args.handle_eat_limit
		&& everyone_ate && every_philosopher_ate_enough(program_state))
	{
		pthread_mutex_lock(&program_state->philosopher_states
			->protect_eat_value);
		program_state->philosopher_states->everyone_ate = TRUE;
		pthread_mutex_unlock(&program_state->philosopher_states
			->protect_eat_value);
		pthread_mutex_unlock(&program_state->philosopher_states->print);
		return (TRUE);
	}
	pthread_mutex_unlock(&program_state->philosopher_states->print);
	return (FALSE);
}

t_bool	everyone_ates(t_program_state *program_state)
{
	pthread_mutex_lock(&program_state->philosopher_states->protect_eat_value);
	if (program_state->philosopher_states->everyone_ate)
	{
		pthread_mutex_unlock(&program_state->philosopher_states
			->protect_eat_value);
		return (TRUE);
	}
	pthread_mutex_unlock(&program_state->philosopher_states->protect_eat_value);
	return (FALSE);
}
