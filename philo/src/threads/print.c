/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nabitbol <nabitbol@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/09 14:18:17 by nabitbol          #+#    #+#             */
/*   Updated: 2021/10/21 18:42:05 by nabitbol         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/philosophers.h"

void	print(int id, int action,
			t_program_state *program_state)
{
	char	*philosopher_messages[4];

	philosopher_messages[TAKES_FORK] = "has taken a fork";
	philosopher_messages[EAT] = "is eating";
	philosopher_messages[SLEEP] = "is sleeping";
	philosopher_messages[THINK] = "is thinking";
	if (!is_philo_died(program_state) && !everyone_ates(program_state))
	{
		pthread_mutex_lock(&program_state->philosopher_states->print);
		printf("%-6lld %d %s\n", get_timestamp() - program_state->args
			.startup_timestamp,
			id + 1, philosopher_messages[action]);
		pthread_mutex_unlock(&program_state->philosopher_states->print);
	}
}
