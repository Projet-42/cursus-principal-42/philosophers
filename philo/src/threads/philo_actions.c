/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_actions.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nabitbol <nabitbol@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/09 14:24:01 by nabitbol          #+#    #+#             */
/*   Updated: 2021/10/21 19:03:49 by nabitbol         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/philosophers.h"

void	philo_eat(t_philosopher_state *philosopher_state,
			t_program_state *program_state)
{
	philosopher_state->last_meal_timestamp = get_timestamp();
	handle_action(philosopher_state, program_state, EAT);
	pthread_mutex_lock(&program_state->philosopher_states->protect_eat_value);
	philosopher_state->number_of_times_philosopher_ate++;
	pthread_mutex_unlock(&program_state->philosopher_states->protect_eat_value);
}

void	philo_sleep(t_philosopher_state *philosopher_state,
				t_program_state *program_state)
{
	handle_action(philosopher_state, program_state, SLEEP);
}

void	philo_think(t_philosopher_state *philosopher_state,
				t_program_state *program_state)
{
	handle_action(philosopher_state, program_state, THINK);
}

void	philo_takes_fork(t_philosopher_state *philosopher_state,
					t_program_state *program_state)
{
	int		next_philosopher_id;

	pthread_mutex_lock(&program_state->protect_value);
	next_philosopher_id = get_next_philospher_id(philosopher_state,
			*program_state);
	pthread_mutex_unlock(&program_state->protect_value);
	if (philosopher_state->thread_id % 2 == 0)
	{
		if (!lock_fork_pair(program_state, philosopher_state,
				next_philosopher_id))
			return ;
	}
	if (philosopher_state->thread_id % 2 != 0)
	{
		if (!lock_fork_unpair(program_state, philosopher_state,
				next_philosopher_id))
			return ;
	}
}
