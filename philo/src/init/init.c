/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nabitbol <nabitbol@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/06 18:21:38 by nabitbol          #+#    #+#             */
/*   Updated: 2021/10/16 22:56:00 by nabitbol         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/philosophers.h"

t_philosopher_state	*create_philosophers_states(int number_of_philosophers,
						t_program_state *program_state)
{
	int					i;
	t_philosopher_state	*philosopher_states;

	philosopher_states = NULL;
	philosopher_states = malloc(sizeof(t_philosopher_state)
			* number_of_philosophers);
	if (philosopher_states == NULL)
		return (NULL);
	i = 0;
	pthread_mutex_init(&philosopher_states->print, NULL);
	pthread_mutex_init(&philosopher_states->protect_eat_value, NULL);
	philosopher_states->death = FALSE;
	philosopher_states->everyone_ate = FALSE;
	while (i < number_of_philosophers)
	{
		pthread_mutex_init(&philosopher_states[i].fork, NULL);
		philosopher_states[i].thread_id = i;
		philosopher_states[i].status = THINK;
		philosopher_states[i].number_of_times_philosopher_ate = 0;
		philosopher_states[i].program_state = program_state;
		philosopher_states[i].last_meal_timestamp = get_timestamp();
		i++;
	}
	return (philosopher_states);
}

t_error	init_error_state(void)
{
	t_error	error;

	error.occured = FALSE;
	error.message = NO_ERROR;
	return (error);
}

void	init(t_program_state *program_state, int ac, char **av)
{
	program_state->error = init_error_state();
	program_state->count_death_print = 0;
	pthread_mutex_init(&program_state->protect_value, NULL);
	pthread_mutex_init(&program_state->protect_death_value, NULL);
	set_error_on_invalid_args(ac, av, &program_state->error);
	if (program_state->error.occured)
		return ;
	program_state->args = parse_args(ac, av);
	program_state->philosopher_states = create_philosophers_states(
			program_state->args.number_of_philosophers,
			program_state);
	program_state->args.startup_timestamp = get_timestamp();
	if (program_state->philosopher_states == NULL)
		set_error(&program_state->error, MEMORY_ALLOCATION_FAILED);
}
