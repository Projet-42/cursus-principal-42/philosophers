/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nabitbol <nabitbol@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/06 18:21:38 by nabitbol          #+#    #+#             */
/*   Updated: 2021/10/12 17:54:58 by nabitbol         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/philosophers.h"

void	is_bigger_than_max_integer(int ac, char **av, t_error *error)
{
	t_bool				is_bigger;
	long unsigned int	tmp;
	int					i;

	i = 1;
	is_bigger = FALSE;
	while (i < ac)
	{
		tmp = ft_atoi(av[i]);
		is_bigger = tmp > 2147483647;
		if (is_bigger)
			set_error(error, MAX_INTEGER);
		i++;
	}
}

void	set_error_on_invalid_args(int ac, char **av, t_error *error)
{
	int		i;

	i = 1;
	if (ac < 5)
		set_error(error, TOO_FEW_ARGS);
	else if (ac > 6)
		set_error(error, TOO_MANY_ARGS);
	is_bigger_than_max_integer(ac, av, error);
	while (i < ac)
	{
		if (!ft_isnumstr(av[i]))
			set_error(error, NOT_A_DIGIT);
		i++;
	}
}

t_args	parse_args(int ac, char **av)
{
	t_args	args;

	args.number_of_philosophers = ft_atoi(av[1]);
	args.time_to_die = ft_atoi(av[2]);
	args.time_to_eat = ft_atoi(av[3]);
	args.time_to_sleep = ft_atoi(av[4]);
	if (ac > 5)
	{
		args.handle_eat_limit = TRUE;
		args.number_of_times_each_philosopher_must_eat = ft_atoi(av[5]);
	}
	else
	{
		args.handle_eat_limit = FALSE;
		args.number_of_times_each_philosopher_must_eat = -1;
	}
	return (args);
}
