/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cleanup.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nabitbol <nabitbol@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/08 16:39:17 by nabitbol          #+#    #+#             */
/*   Updated: 2021/10/16 22:56:24 by nabitbol         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/philosophers.h"

void	cleanup(t_program_state *program_state)
{
	int	i;

	i = 0;
	if (program_state->philosopher_states)
	{
		pthread_mutex_lock(&program_state->protect_value);
		while (i < program_state->args.number_of_philosophers)
		{
			pthread_mutex_destroy(&program_state->philosopher_states[i].fork);
			i++;
		}
		pthread_mutex_destroy(&program_state->philosopher_states->print);
		pthread_mutex_destroy(&program_state->philosopher_states
			->protect_eat_value);
		pthread_mutex_destroy(&program_state->protect_death_value);
		free(program_state->philosopher_states);
		pthread_mutex_unlock(&program_state->protect_value);
		pthread_mutex_destroy(&program_state->protect_value);
	}
}
