/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nabitbol <nabitbol@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/06 18:21:38 by nabitbol          #+#    #+#             */
/*   Updated: 2021/10/11 13:47:41 by nabitbol         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/philosophers.h"

int	main(int ac, char **av)
{
	t_program_state	program_state;

	init(&program_state, ac, av);
	if (has_errored(program_state.error))
		return (0);
	if (program_state.args.number_of_times_each_philosopher_must_eat == 0)
		return (0);
	simulate_philosophers_lives(&program_state);
	cleanup(&program_state);
	return (0);
}
