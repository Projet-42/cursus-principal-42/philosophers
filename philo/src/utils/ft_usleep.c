/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_usleep.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nabitbol <nabitbol@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/16 10:41:42 by nabitbol          #+#    #+#             */
/*   Updated: 2021/10/21 19:13:31 by nabitbol         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/philosophers.h"

void	ft_usleep_eat(t_program_state program_state,
			t_philosopher_state *philosopher_state)
{
	long long	time_elapsed;
	long long	timestamp_begin_eat;

	time_elapsed = 0;
	timestamp_begin_eat = get_timestamp()
		- program_state.args.startup_timestamp;
	while (time_elapsed < program_state.args.time_to_eat)
	{
		usleep(ms_to_us(program_state.args.time_to_eat) / 10000);
		time_elapsed = ((get_timestamp() - program_state.args.startup_timestamp
					- timestamp_begin_eat));
		philosopher_state->last_meal_timestamp = get_timestamp();
	}
}

void	ft_usleep_sleep(t_program_state program_state)
{
	long long	time_elapsed;
	long long	timestamp_begin_sleep;

	time_elapsed = 0;
	timestamp_begin_sleep = get_timestamp()
		- program_state.args.startup_timestamp;
	while (time_elapsed < program_state.args.time_to_sleep)
	{
		usleep(ms_to_us(program_state.args.time_to_sleep) / 10000);
		time_elapsed = ((get_timestamp() - program_state.args.startup_timestamp
					- timestamp_begin_sleep));
	}
}
