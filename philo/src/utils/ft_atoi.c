/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nabitbol <nabitbol@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/05 16:37:37 by nabitbol          #+#    #+#             */
/*   Updated: 2021/10/12 17:39:35 by nabitbol         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

long unsigned int	ft_atoi(const char *str)
{
	int					i;
	long unsigned int	nb;

	i = 0;
	nb = 0;
	while (str[i] >= '0' && str[i] <= '9')
	{
		nb = nb * 10;
		nb = nb + str[i] - '0';
		i++;
	}
	return (nb);
}
