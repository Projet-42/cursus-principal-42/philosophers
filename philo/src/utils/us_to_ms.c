/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   us_to_ms.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nabitbol <nabitbol@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/09 14:20:12 by nabitbol          #+#    #+#             */
/*   Updated: 2021/08/09 14:20:22 by nabitbol         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	us_to_ms(int us)
{
	return (us / 1000);
}
