/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mutex.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nabitbol <nabitbol@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/09 14:34:00 by nabitbol          #+#    #+#             */
/*   Updated: 2021/10/16 22:27:02 by nabitbol         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/philosophers.h"

int	get_next_philospher_id(t_philosopher_state *philosopher_state,
						t_program_state program_state)
{
	int	next_philosopher_id;

	if (philosopher_state->thread_id == (program_state.args
			.number_of_philosophers - 1))
		next_philosopher_id = 0;
	else
		next_philosopher_id = philosopher_state->thread_id + 1;
	return (next_philosopher_id);
}

void	unlock_fork(t_philosopher_state *philosopher_state,
		t_program_state program_state)
{
	int	next_philosopher_id;

	next_philosopher_id = get_next_philospher_id(philosopher_state,
			program_state);
	pthread_mutex_unlock(&program_state.philosopher_states
	[next_philosopher_id].fork);
	pthread_mutex_unlock(&philosopher_state->fork);
}
