/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   has_errored.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nabitbol <nabitbol@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/06 18:21:38 by nabitbol          #+#    #+#             */
/*   Updated: 2021/08/10 09:17:17 by nabitbol         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/philosophers.h"

t_bool	has_errored(t_error error)
{
	char	*error_messages[6];

	error_messages[NO_ERROR] = "No error.";
	error_messages[TOO_MANY_ARGS] = "Too many arguments.";
	error_messages[TOO_FEW_ARGS] = "Too few arguments.";
	error_messages[NOT_A_DIGIT]
		= "Some arguments are not unsigned positive numbers.";
	error_messages[MEMORY_ALLOCATION_FAILED] = "Memory allocation falied.";
	error_messages[MAX_INTEGER]
		= "An argument is bigger than the max integer";
	if (error.occured)
	{
		printf("ERROR: %s\n", error_messages[error.message]);
		return (TRUE);
	}
	return (FALSE);
}
