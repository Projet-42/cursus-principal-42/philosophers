/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ms_to_us.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nabitbol <nabitbol@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/09 14:19:42 by nabitbol          #+#    #+#             */
/*   Updated: 2021/08/10 09:40:04 by nabitbol         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ms_to_us(int ms)
{
	return (ms * 1000);
}
