# Allowed functions

**Knwon functions:**

- write
- printf
- memset
- malloc
- free

**Unknown functions :**

- usleep
    *includes :* "unistd.h"
    *prototype :* int usleep(useconds_t usec); 
    *what it does :* Suspends the execution of the calling thread (during usinc in microseconds).
    *return value :* 0 in success -1 in failure.
- gettimeofday
    *includes :* sys/time.h
    *prototype :*  int gettimeofday(struct timeval *tv, struct timezone *tz);
    *what it does :* get the timezone in tz struct and tv struct
    *return value :* 0 in success and -1 in failure.
- pthread_create
    *includes :* "pthread.h"
    *prototype :* int pthread_create(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine) (void *), void *arg);
    *what it does :* Create a new thread by callin a function
    *return value :* 0 on sucess, a number on failure.
- pthread_detach
    *includes :* "pthread.h"
    *prototype :* int pthread_detach(pthread_t thread);
    *what it does :* mark the thread identified by thread as detached. When a detached thread terminates its ressources are automatically released basck to the system without need for another thread to join.
    *return value :* 0 on success, a number on failure.
- pthread_join
    *includes :* "pthread.h"
    *prototype :* int pthread_join(pthread_t thread, void **retval);
    *what it does :* Wait to the thread specified by thread to terminate.
    *return value :* 0 on success, a number on failure.
- pthread_mutex_init
    *includes :* "pthread.h"
    *prototype :* int pthread_mutex_init(pthread_mutex_t *restrict mutex,
const pthread_mutexattr_t *restrict attr);
    *what it does :* The pthread_mutex_init() function shall initialize the mutex referenced by mutex with attributes specified by attr. If attr is NULL, the default mutex attributes are used; the effect shall be the same as passing the address of a default mutex attributes object. Upon successful initialization, the state of the mutex becomes initialized and unlocked. 
    *return value :* 0 on success, a number on failure.
- pthread_mutex_destroy
    *includes :* "pthread.h"
    *prototype :* int pthread_mutex_destroy(pthread_mutex_t *mutex);
    *what it does :* The pthread_mutex_destroy() function shall destroy the mutex object referenced by mutex; the mutex object becomes, in effect, uninitialized. An implementation may cause pthread_mutex_destroy() to set the object referenced by mutex to an invalid value. A destroyed mutex object can be reinitialized using pthread_mutex_init(); the results of otherwise referencing the object after it has been destroyed are undefined. 
    *return value :* 0 on success, a number on failure.
- pthread_mutex_lock
    *includes :* "pthread.h"
    *prototype :* int pthread_mutex_lock(pthread_mutex_t *mutex); 
    *what it does :* The mutex object referenced by mutex shall be locked by calling pthread_mutex_lock(). If the mutex is already locked, the calling thread shall block until the mutex becomes available. This operation shall return with the mutex object referenced by mutex in the locked state with the calling thread as its owner. 
    *return value :* 0 on success, a number on failure.
- pthread_mutex_unlock
    *includes :* "pthread.h"
    *prototype :* int pthread_mutex_unlock(pthread_mutex_t *mutex); 
    *what it does :* The pthread_mutex_unlock() function shall release the mutex object referenced by mutex. The manner in which a mutex is released is dependent upon the mutex's type attribute. If there are threads blocked on the mutex object referenced by mutex when pthread_mutex_unlock() is called, resulting in the mutex becoming available, the scheduling policy shall determine which thread shall acquire the mutex. 
    *return value :* 0 on success, a number on failure.

mutex : pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER; 