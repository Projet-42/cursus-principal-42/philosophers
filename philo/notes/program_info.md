## Rules

> there is one fork between each philosopher (at right and left).
    - For two philosopher, there is two fork.

> Philosophers need two forks to eat so the philosopher N - 1 and the philosopher N + 1 can't eat (protect the fork using a mutex);

> Philosopher must never be starving. So we have to be aware about the starving (time_to_die) of the N - 1 and N + 1 of the eaten philosopher.

## Data

### All variables

#### Parsing
- philo_number;     -> int
- fork;             -> int
- philo_eat;        -> float (milliseconds)
- philo_sleep;      -> float (milliseconds)
- philo_starv;      -> float (milliseconds)

#### Thread
- mutex;
- philo_state;      -> double pointer on char (eat/sleep/think/dead/has_fork)
- died;             -> bool  (alive/dead)
- philo_had_eaten   -> int   (number of time for the option [ ’number_of_times_each_philosopher_must_eat])

### Data structure

- args
  - number_of_philosophers
  - time_to_die
  - time_to_eat
  - time_to_sleep
  - number_of_times_each_philosopher_must_eat
- [philosopher_state]
  - status **char**
  -	number_of_times_philosopher_ate
