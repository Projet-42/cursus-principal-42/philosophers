# Philosophers

## What is this project

Philosophers is a simplified "philosophers" life simulation in c. It simulate
the life of people called philosophers who are around a round table, they:

- Sleep
- Eat
- Think
- Die (maybe)

When a philosopher dies the simulation stop.

## How it works

This program is written in C and uses the `pthread` library, it creates multiple
threads that are used to simulate the actions of one philosopher each.

> **Process**: A process is a program in execution, it can spawn other processes
> known as *child processes*. Processes does not share memory between
> them.

> **Thread**: A thread is a **part of a program** that can be executed
> concurently and share memory with another part of that same program (another
> thread). Threads belong to a process.

### Learn more
- [Threads vs Processes](https://www.geeksforgeeks.org/difference-between-process-and-thread/)
- Multithreading in C, see [here](https://www.tutorialspoint.com/multithreading-in-c) and [here](https://www.geeksforgeeks.org/multithreading-c-2/)

## Quick start

```sh
make && ./philo 5 1000 400 200 3
```

### Usage

```sh
./philo number_of_philosophers time_to_die time_to_eat time_to_sleep [number_of_times_each_philosopher_must_eat]
```

- `number_of_philosophers`: is the number of philosophers and also the number
of forks

- `time_to_die`: is in milliseconds, if a philosopher doesn't start eating
`time_to_die` milliseconds after starting his last meal or the beginning of the
simulation, it dies

- `time_to_eat`: is in milliseconds and is the time it takes for a philosopher
to eat. During that time he will need to keep the two forks.

- `time_to_sleep`: is in milliseconds and is the time the philosopher will spend
sleeping.

- `number_of_times_each_philosopher_must_eat`: argument is optional, if all
philosophers eat at least `number_of_times_each_philosopher_must_eat` the
simulation will stop. If not specified, the simulation will stop only at the
death of a philosopher.
